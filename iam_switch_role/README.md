<!-- BEGIN_AUTOMATED_TF_DOCS_BLOCK -->
## Requirements

No requirements.

Basic usage of this module is as follows:

```hcl
module "example" {
	 source  = "<module-path>"

	 # Required variables
	 name  = 

	 # Optional variables
	 assume_external_id  = ""
	 assume_type  = "CONSOLE"
	 policy_attachment_arns  = []
	 source_account_id  = "767398087110"
}
```

## Resources

| Name | Type |
|------|------|
| [aws_iam_role.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_policy_document.assume_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.assume_role_policy_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_assume_external_id"></a> [assume\_external\_id](#input\_assume\_external\_id) | n/a | `string` | `""` | no |
| <a name="input_assume_type"></a> [assume\_type](#input\_assume\_type) | n/a | `string` | `"CONSOLE"` | no |
| <a name="input_name"></a> [name](#input\_name) | n/a | `string` | n/a | yes |
| <a name="input_policy_attachment_arns"></a> [policy\_attachment\_arns](#input\_policy\_attachment\_arns) | n/a | `list(string)` | `[]` | no |
| <a name="input_source_account_id"></a> [source\_account\_id](#input\_source\_account\_id) | n/a | `string` | `"767398087110"` | no |
## Outputs

| Name | Description |
|------|-------------|
| <a name="output_role_arn"></a> [role\_arn](#output\_role\_arn) | n/a |
| <a name="output_role_name"></a> [role\_name](#output\_role\_name) | n/a |
<!-- END_AUTOMATED_TF_DOCS_BLOCK -->
<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_role.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_policy_document.assume_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.assume_role_policy_api](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_assume_external_id"></a> [assume\_external\_id](#input\_assume\_external\_id) | n/a | `string` | `""` | no |
| <a name="input_assume_type"></a> [assume\_type](#input\_assume\_type) | n/a | `string` | `"CONSOLE"` | no |
| <a name="input_name"></a> [name](#input\_name) | n/a | `string` | n/a | yes |
| <a name="input_policy_attachment_arns"></a> [policy\_attachment\_arns](#input\_policy\_attachment\_arns) | n/a | `list(string)` | `[]` | no |
| <a name="input_source_account_id"></a> [source\_account\_id](#input\_source\_account\_id) | n/a | `string` | `"767398087110"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_role_arn"></a> [role\_arn](#output\_role\_arn) | n/a |
| <a name="output_role_name"></a> [role\_name](#output\_role\_name) | n/a |
<!-- END_TF_DOCS -->