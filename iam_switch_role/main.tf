resource "aws_iam_role" "this" {
  name               = var.name
  assume_role_policy = var.assume_type == "API" ? data.aws_iam_policy_document.assume_role_policy_api.json : data.aws_iam_policy_document.assume_role_policy.json
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${var.source_account_id}:root"]
    }

    condition {
      test     = "Bool"
      variable = "aws:MultiFactorAuthPresent"
      values   = ["true"]
    }
  }
}

data "aws_iam_policy_document" "assume_role_policy_api" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${var.source_account_id}:root"]
    }

    condition {
      test     = "StringEquals"
      variable = "sts:ExternalId"
      values   = [var.assume_external_id]
    }
  }
}

resource "aws_iam_role_policy_attachment" "this" {
  for_each   = toset(var.policy_attachment_arns)
  role       = aws_iam_role.this.name
  policy_arn = each.key
}
