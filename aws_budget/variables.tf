variable "resource_prefix" {
  type = string
}

variable "cost_tags" {
  type = list(string)
  default = [
    "project",
    "environment",
    "service",
    "region",
  ]
}

variable "budget_limit" {
  type    = number
  default = 4000
}

