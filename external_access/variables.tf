variable "name" {
  type = string
}

variable "source_account_id" {
  type = string
}

variable "policy_attachment_arns" {
  type    = list(string)
  default = []
}

variable "assume_type" {
  type    = string
  default = "CONSOLE"

  validation {
    condition     = var.assume_type == "API" || var.assume_type == "CONSOLE"
    error_message = "Allow values for assume_type are: API, CONSOLE"
  }
}

variable "assume_external_id" {
  type    = string
  default = "some-secret-random-string"
}
