data "aws_vpc_peering_connection" "this" {
  count      = var.peering_mode == "ACCEPTER" ? 1 : 0
  vpc_id     = var.peer_vpc_id
  owner_id   = var.peer_owner_id
  cidr_block = var.peer_cidr_block
}

resource "aws_vpc_peering_connection_accepter" "this" {
  count                     = var.peering_mode == "ACCEPTER" ? 1 : 0
  vpc_peering_connection_id = data.aws_vpc_peering_connection.this[0].id
  auto_accept               = true

  tags = {
    type = "accepter"
    Name = var.name
  }
}

resource "aws_vpc_peering_connection" "this" {
  count = var.peering_mode == "REQUESTER" ? 1 : 0

  peer_owner_id = var.peer_owner_id
  peer_vpc_id   = var.peer_vpc_id
  vpc_id        = var.vpc_id

  peer_region = var.aws_region
  auto_accept = false

  tags = {
    Name = var.name
  }
}

resource "aws_route" "this" {
  for_each = toset(var.route_table_ids)

  route_table_id            = each.value
  destination_cidr_block    = var.peer_cidr_block
  vpc_peering_connection_id = var.peering_mode == "ACCEPTER" ? data.aws_vpc_peering_connection.this[0].id : aws_vpc_peering_connection.this[0].id
}
