variable "name" {
  type = string
}

variable "aws_region" {
  type = string
}

variable "peering_mode" {
  type    = string
  default = "ACCEPTER"
}

variable "peer_owner_id" {
  type = string
}

variable "peer_vpc_id" {
  type = string
}

variable "peer_cidr_block" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "route_table_ids" {
  type    = list(string)
  default = []
}
